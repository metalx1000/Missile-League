extends Camera3D

@export var lerp_speed = 3.0
@export var offset = Vector3.ZERO
var target
var id = 0
var ball_look = false

@onready var ball = get_tree().get_first_node_in_group("balls")
@onready var focus_point = Node3D.new()

func _ready():
	
	get_tree().get_root().add_child(focus_point)
	pass
	
func _physics_process(delta):
	if !target:
		return

	var target_pos = target.global_transform.translated_local(offset)
	
	if ball_look:
		focus_point.position = lerp(focus_point.position,ball.position,delta)
		target_pos = target.global_transform.translated_local(Vector3(0,2,0))

	else:
		focus_point.position = lerp(focus_point.position,target.position,.8)

	
	global_transform = global_transform.interpolate_with(target_pos, lerp_speed * delta)
	look_at(focus_point.position, Vector3.UP)
	

func _input(event):
	if event.device != Global.controllers[id]:
		return
	
	if event.is_action_pressed("look_at"):
		if ball_look:
			ball_look = false
		else:
			ball_look = true
	
