extends Node3D

@export var id = 0
@onready var point_wall = $point_wall
var activate = false
func _ready():
	if id == 1:
		$blue.visible = false
	else:
		$blue.visible = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func score(body):
	if !body.is_in_group("balls") || activate:
		return
	$score.play()
	$cheering.play()
	activate = true
	Global.scores[id] += 1
	$reload.start()


func _on_reload_timeout():
	var balls = get_tree().get_nodes_in_group("balls")
	for ball in balls:
		ball.queue_free()
	get_tree().reload_current_scene()
