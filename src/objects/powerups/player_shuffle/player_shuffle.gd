extends Node3D

var start = 0
var id = 0
var parent 

func _ready():
	switch()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func effect():
	var viewports = get_tree().get_nodes_in_group("viewport_containers")
	for vp in viewports:
		vp.flash()

func switch():
	effect()
	var players = get_tree().get_nodes_in_group("players")
	var cameras = get_tree().get_nodes_in_group("cameras")
	var positions = []
	for p in players:
		positions.append(p.position)
		
	for i in range(players.size()):
		start += 1
		if start >= players.size():
			start = 0
		
		players[i].position = positions[start]
		cameras[i].position = cameras[start].position
