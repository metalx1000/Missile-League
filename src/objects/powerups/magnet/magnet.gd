extends Node3D

var power = 5
var id
var balls
var ball
var parent 
@export var magnet_force = .5

@onready var bolt_group = $magnet/bolt_group
var on = false
func _ready():
	ball = get_tree().get_first_node_in_group("balls")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	$magnet.look_at(ball.position)
	if power <= 0:
		on = false
		queue_free()
		
	if on:
		power_on(delta)
	else:
		power_off()
		
func power_on(delta):
	power -= delta
	if !$sound_effect.playing:
		$sound_effect.play()
	bolt_group.visible = true
	ball.magnet = parent

func power_off():
	$sound_effect.stop()
	bolt_group.visible = false
	ball.magnet = null
	ball.magnet_force = magnet_force
	
func _input(event):
	if event.device != Global.controllers[id]:
		return
	
	if event.is_action_pressed("trigger"):
		on = true
		
	if event.is_action_released("trigger"):
		on = false


