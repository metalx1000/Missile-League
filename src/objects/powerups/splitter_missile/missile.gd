extends Node3D

var balls = preload("res://objects/ball/ball.tscn")
var velocity = Vector3.ZERO
var muzzle_velocity = 50
var g = Vector3.DOWN * 20
var dead = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _physics_process(delta):
	#velocity += g * delta
	if dead:
		return
		
	look_at(transform.origin + velocity.normalized(), Vector3.UP)
	transform.origin += velocity * delta

func _on_area_3d_body_entered(body):
	if dead:
		return
	
	dead = true
	visible = false
	if body.is_in_group("balls"):
		var ball = balls.instantiate()
		ball.transform = body.global_transform
		get_tree().get_root().add_child(ball)
		
	$AnimationPlayer.play("death")
