extends Node3D

var id = 0
var balls
var ball
var parent 
var shots = 3
var missile = preload("res://objects/powerups/splitter_missile/missile.tscn")

func _ready():
	ball = get_tree().get_first_node_in_group("balls")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	$gun.look_at(ball.position)


func _input(event):
	if event.device != Global.controllers[id]:
		return
	
	if event.is_action_pressed("trigger"):
		shots -= 1
		var projectile = missile.instantiate()
		#projectile.global_position = $gun/barrel.global_position
		get_tree().get_root().add_child(projectile)
		projectile.transform = $gun/barrel.global_transform
		projectile.velocity = -projectile.transform.basis.z * projectile.muzzle_velocity
		if shots <= 0:
			queue_free()
		
	if event.is_action_released("trigger"):
		pass
