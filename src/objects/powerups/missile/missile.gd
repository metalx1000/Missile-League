extends Node3D

var explosions = preload("res://objects/explosion/explosion_02.tscn")
var velocity = Vector3.ZERO
var offset = Vector3.ZERO
var speed = 30

var dead = false
var target

func _ready():
	look_at(target.position)
	pass # Replace with function body.


func _physics_process(delta):
	#velocity += g * delta
	if dead:
		return
	
	var target_pos = target.global_transform.translated_local(offset)
	#look_at(transform.origin + velocity.normalized(), Vector3.UP)

	look_at(target.position)	
	position -= (position - target.position).normalized() * speed * delta
	#global_transform = global_transform.interpolate_with(target_pos, lerp_speed * delta)

func _on_area_3d_body_entered(body):
	if dead:
		return
	
	dead = true
	visible = false
	#if body.is_in_group("take_damage"):
	var explosion = explosions.instantiate()
	explosion.transform = body.global_transform
	get_tree().get_root().add_child(explosion)
		
	$AnimationPlayer.play("death")
