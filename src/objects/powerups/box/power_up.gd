extends Node3D

var active = true
var magenet = preload("res://objects/powerups/magnet/magnet.tscn")
var magenet_repel = preload("res://objects/powerups/magnet/magnet_repel.tscn")
var missile_splitter = preload("res://objects/powerups/splitter_missile/splitter_missile.tscn")
var missile_gun = preload("res://objects/powerups/missile/missile_gun.tscn")
var player_shuffle = preload("res://objects/powerups/player_shuffle/player_shuffle.tscn")

var power_ups = [
	magenet,
	missile_splitter,
	player_shuffle,
	magenet_repel,
	missile_gun
]


# Called when the node enters the scene tree for the first time.
func _ready():
	#power_ups = [ missile_gun ]
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_y(5*delta)
	rotate_x(1*delta)


func _on_area_3d_body_entered(body):
	if !body.is_in_group("players") || !active:
		return
	
	active = false
	$collect.play()
	visible = false
	
	randomize()
	power_ups.shuffle()
	
	var item = power_ups[0].instantiate()
	item.id = body.id
	item.parent = body
	for child in body.powerup_mount.get_children():
		child.queue_free()
		
	body.powerup_mount.add_child(item)
	$Reset_Timer.start()

func _on_reset_timer_timeout():
	active = true
	visible = true
