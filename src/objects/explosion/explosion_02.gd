extends Node3D

@onready var mesh = $MeshInstance3D
var dead = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func deactivate():
	dead = true
	
func _on_area_3d_body_entered(body):
	if dead:
		return
		
	if !body.is_in_group("take_damage"):
		return
	var push = Vector3(1,1,1)
	
	if body.is_in_group("players"):
		push = Vector3(20,20,20)
		body.apply_impulse(push)
		body.apply_torque_impulse(push)
		body.linear_velocity = push
		return
		
	body.apply_impulse(push)
	
	
