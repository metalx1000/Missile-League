extends VehicleBody3D

@export var id = 0
@export var car_name = ""

const MAX_STEER = 0.2
const ENGINE_POWER = 150
@onready var ground_ray = $ground_ray
@onready var jump_timer = $jump_timer
@onready var powerup_mount = $powerup_mount
var double_jump = true
var jump_power = 15
var boost_level_max = 50
var boost_level = 50
var boost_enabled = false
var crash = false
var active = true

# Called when the node enters the scene tree for the first time.
func _ready():
	
	add_to_group("take_damage")
	center_of_mass = $center_mass.position
	
func _physics_process(delta):
	if !active:
		return
	#check_if_upsidedown()
	update_boost(delta)
	out_of_bounds()


	
func _input(event):
	if !active:
		return
		
	if event.device != Global.controllers[id]:
		return
	
	steer(.2)
	booster()
	jump()
	
func out_of_bounds():
	if position.y < -50:
		linear_velocity = Vector3(0,0,0)
		position = Vector3(0,5,0)
		
func booster():
	if !Input.is_action_pressed("boost") || boost_level <= 0:
		boost_enabled = false
		$sounds/booster.stop()
		
		return

	if !$sounds/booster.playing:
			$sounds/booster.play()
			
	boost_enabled = true

	
func update_boost(delta):
	if boost_level <= 0:
		boost_enabled = false
		$sounds/booster.stop()
		
	var camera = get_tree().get_nodes_in_group("cameras")[id]
	if boost_enabled:
		camera.lerp_speed = 10
		engine_force -= 10
		boost_level -= delta * 5
		engine_force = clamp(engine_force,0,-1000)
		if linear_velocity.length() * 2 > 180:
			engine_force = ENGINE_POWER * delta
	else:
		if !Input.is_action_pressed("boost"):
			boost_level += delta * 2
		camera.lerp_speed = 3
		if linear_velocity.length() * 2 > 80:
			engine_force = ENGINE_POWER * delta
		
	boost_level = clamp(boost_level,0,boost_level_max)
		
func steer(delta):
	steering = move_toward(steering, Input.get_axis("steer_right","steer_left") * MAX_STEER, delta *2.5)
	engine_force = Input.get_axis("accelerate","brake") * ENGINE_POWER 

	if abs(engine_force) !=0:
		if !$sounds/engine.playing:
			$sounds/engine.play()
		$sounds/idle.stop()
	else:
		if !$sounds/idle.playing:
			$sounds/idle.play()
		$sounds/engine.stop()

func jump():
	if Input.is_action_just_pressed("jump"):
		flip()
		jump_timer.start()
		if ground_ray.is_colliding():
			$sounds/jump.play()
			#linear_velocity = Vector3(0,10,0)
			double_jump = true
			linear_velocity.y = jump_power
		elif double_jump:
			$sounds/jump.play()
			double_jump = false
			linear_velocity.y = jump_power


func flip():
	#if stuck on side or upside down flip
	if abs(linear_velocity.x) > 1:
		return
	
	if abs(linear_velocity.y) > 1:
		return
	
	if abs(linear_velocity.z) > 1:
		return
	
	if Input.is_action_pressed("accelerate"):
		angular_velocity = Vector3(0,0,10)
	
func _on_jump_timer_timeout():
	double_jump = true


func _on_crash_body_entered(body):
	if !body.is_in_group("players") || !crash:
		crash = true
		return
	if !$sounds/crash.playing:
		$sounds/crash.play()
