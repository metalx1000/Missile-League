extends RigidBody3D
var magnet
var magnet_force = 0.0

var hit_vel = 5
# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("take_damage")
	randomize()
	var amount = 10
	var spin = randf_range(-amount,amount)
	apply_torque(Vector3(spin,0,0))

func _process(delta):

	out_of_bounds()

func _integrate_forces(state):
	
	#set_scale(Vector3(.5, .5, .5))
	if magnet != null:
		linear_velocity = (magnet.global_position - global_transform.origin) * magnet_force
	
func _on_body_entered(body):
	#print(abs(linear_velocity))
	if abs(linear_velocity.x) > hit_vel || abs(linear_velocity.y) > hit_vel || abs(linear_velocity.z) > hit_vel:
		$hit_snd.play()

func out_of_bounds():
	if position.y < -50:
		linear_velocity = Vector3(0,0,0)
		position = Vector3(0,50,0)
