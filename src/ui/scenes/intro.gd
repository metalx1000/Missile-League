extends Control

@onready var player = $AnimationPlayer

var current = 0

@onready var pages = [
	$page_01, $page_02, $page_03
]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		$AnimationPlayer.stop()
		$AnimationPlayer.play("fade")

func next():
	if current > pages.size() - 1:
		get_tree().change_scene_to_file("res://ui/controls/controller_setup.tscn")
		return
	
	for p in pages:
		p.visible = false
		
	
	pages[current].visible = true
	current += 1
