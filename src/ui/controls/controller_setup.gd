extends Control

var controller = 0
var count = -1
var last_controller: int = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$red_ball_01.rotate_y(0.1 * delta)
	if controller < 2:
		$msg.text = "Press a Button Twice\non Controller #" + str(controller+1)
	
func _input(event):
	if !event.is_pressed():
		return
		
	var device = event.device
	$button_snd.play()
	if device == last_controller:
		count += 1
	else:
		count = 0
		
	last_controller = device
	
	if count >= 1:
		Global.controllers.append(device)
		controller += 1
		last_controller = -1
		#print("Player #" + str(controller+1) + " is device #" + str(device))
	
	if controller > 1:
		$exit.play("exit")
		
func next():
	get_tree().change_scene_to_file("res://maps/level_01.tscn")
