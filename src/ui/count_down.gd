extends CenterContainer

var current_count = 3
@onready var msg = $Label
# Called when the node enters the scene tree for the first time.
func _ready():
	msg.text = str(current_count)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func down():
	current_count -= 1
	msg.text = str(current_count)
	if current_count <= 0:
		msg.visible = false
		$AnimationPlayer.play("done")
		
func start():
	for c in get_tree().get_nodes_in_group("players"):
		c.active = true
