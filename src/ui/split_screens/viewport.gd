extends SubViewportContainer
@onready var camera = $SubViewport/Camera3D
@export var id = 0

@onready var boost_label = $SubViewport/HUD/BOOST
@onready var target = get_tree().get_nodes_in_group("players")[id]
@onready var msg = $SubViewport/HUD/msg
@onready var msg_timeout = $msg_timeout
@onready var animation_player = $AnimationPlayer
# Called when the node enters the scene tree for the first time.
func _ready():
	
	camera.id = id
	camera.target = target
	msg.text = target.car_name



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	hud_update()
	
func hud_update():
	var boost_level = target.boost_level
	boost_label.text = "BOOST: " + str(round(boost_level))
	boost_label.text += "\nSPEED: " + str(round(target.linear_velocity.length()*2))
	boost_label.text += "\nSCORE: " + str(Global.scores[id])

func flash():
	animation_player.play("flash")
	
func _on_msg_timeout_timeout():
	msg.text = ""
