extends Node

var music_player = AudioStreamPlayer.new()

var controllers = []

var music = [
	"res://music/Basic Metal 24 - Heavy Metal - Royalty Free Music [DAJTX6HqgEM].ogg",
	"res://music/Basic_Metal_25.ogg",
	"res://music/Frankensynth - Dubstep⧸Chillstep - Royalty Free Music [yx0LJG1TQfg].ogg",
	"res://music/Rolling Low - Alternative Metal - Royalty Free Music [buFCb-5X1u4].ogg", "res://music/Step Into the Unknown - Breakbeat - Royalty Free Music [w3448YuSGgM].ogg",
	"res://music/When the Signal Goes Down - Drum and Bass⧸Action - Royalty Free Music [NDXkje2YYPU].ogg"
]



var scores = [ 0, 0]
# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	add_child(music_player)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()

func load_music(file):
	if ResourceLoader.exists(file):
		var song = load(file) 
		song.set_loop(true)
		music_player.stream = song
		music_player.play()
	
func random_song():
	randomize()
	var songs = music
	songs.shuffle()
	return songs[0]
