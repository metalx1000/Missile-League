extends Node

@onready var music = $music


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	var song = Global.random_song()
	load_music(song)
	for c in get_tree().get_nodes_in_group("players"):
		c.active = false
	
func _process(delta):

	pass

func _input(event):
	if event.is_action_pressed("change_music"):
		var song = Global.random_song()
		load_music(song)

func load_music(file):
	if ResourceLoader.exists(file):
		var song = load(file) 
		song.set_loop(true)
		$music.stream = song
		$music.play()


func _on_timer_timeout():
	$start_sound.play()
