# Missile-League

Copyright Kris Occhipinti 2023-10-16

(https://filmsbykris.com)

License GPLv3

# Music
By https://teknoaxe.com/
Licensed under a Creative Commons Attribution 4.0 International License
Basic Metal 25 - https://teknoaxe.com/Link_Code_3.php?q=2015&genre=Rock

# Sounds
Sound Effect by <a href="https://pixabay.com/users/alex_kizenkov-33612407/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=141942">Alex_Kizenkov</a> from <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=141942">Pixabay</a>

